name := "akka-dynamodb"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "com.lightbend.akka" %% "akka-stream-alpakka-dynamodb" % "1.1.0",
  "org.scanamo" %% "scanamo" % "1.0.0-M10",
  "org.scanamo" %% "scanamo-alpakka" % "1.0.0-M10",
  "org.scanamo" %% "scanamo-testkit" % "1.0.0-M10"

)
