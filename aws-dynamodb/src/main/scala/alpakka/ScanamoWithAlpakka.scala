package alpakka

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.dynamodb.{DynamoClient, DynamoSettings}
import akka.stream.scaladsl.{Keep, RunnableGraph, Sink}
import akka.Done
import org.scanamo.{DynamoFormat, ScanamoAlpakka, Table}
import org.scanamo.semiauto._

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

object ScanamoWithAlpakka extends App {

  implicit val system: ActorSystem = ActorSystem("alpakka-dynamodb")
  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext = system.dispatcher

  object model {
    case class Client(`pk_`: String, id: String, name: String, info: Map[String, String])
  }

  implicit val formatClient: DynamoFormat[model.Client] =
    deriveDynamoFormat[model.Client]
  val table = Table[model.Client]("confserver.hmg.platform")

  val ops = for {
    scanResult <- table.scan()

  } yield scanResult

  val alpakkaClient = DynamoClient(DynamoSettings.create(system))

  val scanamoAlpakka: ScanamoAlpakka = ScanamoAlpakka(alpakkaClient)
  val source = scanamoAlpakka.exec(ops)

  val r: RunnableGraph[Future[Done]] =
    source.toMat(Sink.foreach(x => println(s"---- $x")))(Keep.right)
  r.run().foreach(_ => println("-----------------"))

  Await.result(system.whenTerminated, Duration.Inf)
}
