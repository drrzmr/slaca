package hello

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.Http.IncomingConnection
import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.Uri
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink

import scala.util.Failure
import scala.util.Success

object HttpServer extends App {

  implicit val system: ActorSystem             = ActorSystem("AkkaHttpHello")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  import system.dispatcher

  val requestFlow: Flow[HttpRequest, HttpResponse, _] = Flow[HttpRequest].map {

    case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
      HttpResponse(
        entity = HttpEntity(
          ContentTypes.`text/plain(UTF-8)`,
          "welcome to akka upside down!\n"
        )
      )

    case HttpRequest(GET, Uri.Path("/about"), _, _, _) =>
      HttpResponse(
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   <h1> kaak </h1>
            | </body>
            |</html>
          """.stripMargin + "\n"
        )
      )

    case request: HttpRequest =>
      request.discardEntityBytes()
      HttpResponse(StatusCodes.NotFound, entity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, "not found\n"))
  }

  val connectionSink = Sink.foreach[IncomingConnection] { connection =>
    println(s"Accepted incoming connection from: ${connection.remoteAddress}")
    connection.handleWith(requestFlow)
  }

  val serverSource = Http().bind("localhost", 8388)

  val bindingFuture = serverSource.to(connectionSink).run()

  bindingFuture.onComplete {
    case Failure(exception) => throw exception
    case Success(binding)   => println(s"Server binding successful on: ${binding.localAddress}")
  }

  /*
   * shutdown akka system
   */
  //bindingFuture
  //  .flatMap { binding => binding.unbind() }
  //  .onComplete(_ => system.terminate())
}
