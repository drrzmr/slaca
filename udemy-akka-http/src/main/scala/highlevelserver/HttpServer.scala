package highlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.Done
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import highlevelserver.HttpServer.CreatePeople
import highlevelserver.HttpServer.Error
import highlevelserver.HttpServer.People
import spray.json.DefaultJsonProtocol
import spray.json.RootJsonFormat
import spray.json._

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration

trait JsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val peopleFormat: RootJsonFormat[People]             = jsonFormat2(People)
  implicit val createPeopleFormat: RootJsonFormat[CreatePeople] = jsonFormat1(CreatePeople)
  implicit val errorFormat: RootJsonFormat[Error]               = jsonFormat1(Error)

}

object HttpServer extends App with JsonProtocol {

  implicit val system: ActorSystem = ActorSystem("HighLevelExercise")
  implicit val mat: Materializer   = ActorMaterializer()
  import system.dispatcher

  case class Error(message: String)
  type Name = String
  case class People(pin: Int, name: Name)
  case class CreatePeople(name: Name)

  def peopleCreatorFactory(pinSeed: Int = 0) = {
    var seed = pinSeed

    def pin: Int = {
      val p = seed
      seed += 1
      p
    }

    def creator(newPeople: CreatePeople): People = People(pin, newPeople.name)
    creator _
  }

  val peopleCreator = peopleCreatorFactory(69)

  val names   = List("lemmy", "maze", "eder")
  var peoples = names.map(name => peopleCreator(CreatePeople(name)))

  val route: Route =
    pathPrefix("api" / "people") {
      concat(
        get {
          concat(
            path(IntNumber) { pin =>
              peoples.find(people => people.pin == pin) match {
                case None         => complete(StatusCodes.NotFound, Error(s"pin: '$pin' not found"))
                case Some(people) => complete(StatusCodes.OK, people)
              }
            },
            (pathEndOrSingleSlash & parameter('pin.as[Int])) { pin =>
              peoples.find(people => people.pin == pin) match {
                case None         => complete(StatusCodes.NotFound, Error(s"pin: '$pin' not found"))
                case Some(people) => complete(StatusCodes.OK, people)
              }
            },
            pathEndOrSingleSlash {
              complete(StatusCodes.OK, peoples.toJson)
            }
          )
        },
        post {
          entity(as[CreatePeople]) { createPeople =>
            val newPeople = peopleCreator(createPeople)
            peoples = newPeople :: peoples
            complete(StatusCodes.OK, newPeople)
          }
        }
      )
    }

  val source: Source[Http.IncomingConnection, Future[Http.ServerBinding]] =
    Http().bind("localhost", 8080)

  val sink: Sink[Http.IncomingConnection, Future[Done]] =
    Sink.foreach[Http.IncomingConnection] { connection =>
      connection.handleWith(route)
    }

  val bindingFuture: Future[Http.ServerBinding] = source.to(sink).run()
  Await.result(bindingFuture, Duration.Inf)
}
