package store

import akka.http.scaladsl.model._
import spray.json.JsValue

class Response(entity: ResponseEntity) {
  lazy val asOk: HttpResponse         = HttpResponse(StatusCodes.OK, entity = entity)
  lazy val asBadRequest: HttpResponse = HttpResponse(StatusCodes.BadRequest, entity = entity)
  lazy val asNotFound: HttpResponse   = HttpResponse(StatusCodes.NotFound, entity = entity)

}

object Response {
  def apply(entity: ResponseEntity): Response = new Response(entity)

  def apply(text: String): Response = new Response(
    HttpEntity(ContentTypes.`text/plain(UTF-8)`, text)
  )

  def apply(exception: Exception): Response = new Response(
    HttpEntity(ContentTypes.`text/plain(UTF-8)`, exception.getMessage)
  )

  def apply(json: JsValue): Response = new Response(
    HttpEntity(ContentTypes.`application/json`, json.compactPrint)
  )

}
