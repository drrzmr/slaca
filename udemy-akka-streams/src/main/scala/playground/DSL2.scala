package playground

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.ClosedShape
import akka.NotUsed
import akka.stream.scaladsl.Balance
import akka.stream.scaladsl.GraphDSL
import akka.stream.scaladsl.Merge
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

object DSL2 extends App {

  implicit val system: ActorSystem             = ActorSystem("DSL2")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val fastSource = Source(0 to 20).throttle(5, 1 second)
  val slowSource = Source(10000 to (10000 + 20)).throttle(2, 1 seconds)

  val firstSink = Sink.foreach[Int] { a =>
    println(s"a: $a")
  }

  val secondSink = Sink.foreach[Int] { b =>
    println(s"b: $b")
  }

  val graph: RunnableGraph[NotUsed] = RunnableGraph.fromGraph(
    GraphDSL.create() { builder =>
      val merge   = builder.add(Merge[Int](2))
      val balance = builder.add(Balance[Int](2))

      import GraphDSL.Implicits._
      implicit val i = builder
      fastSource ~> merge
      slowSource ~> merge
      merge ~> balance
      balance ~> firstSink
      balance ~> secondSink

      ClosedShape
    }
  )

  graph.run()

  //system.terminate
  Await.result(system.whenTerminated, Duration.Inf)
}
